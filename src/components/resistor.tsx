import {Component} from 'react'


//styles
import Image from 'react-bootstrap/Image'
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup'
import '../styles/stripes.css';

// export interface colorStripes {
// 	first:string,
// 	second: string,
// 	multiplier: string,
// 	tolerance: string
// }
export default function Resistor(props: any)  {
	// console.log("props.colorStripes", props.colorStripes);
	const stripes = props.colorStripes;

	return (
		<div >

	        <Container>
	          <Row>
	          	<Col data-testid="first-color" className={stripes.first}>{stripes.first}</Col>
	          	<Col data-testid="second-color" className={stripes.second}>{stripes.second}</Col>
	          	<Col data-testid="third-color" className={stripes.multiplier}>{stripes.multiplier}</Col>
	          	<Col data-testid="fourth-color" className={stripes.tolerance}>{stripes.tolerance}</Col>
	          </Row>
	          <br />
	          <Card style={{ width: '18rem' }}>
			  	<ListGroup variant="flush">
			    	<ListGroup.Item data-testid="resistance-value"><b>Resistance: </b> {stripes.resistance} ohms</ListGroup.Item>
			    	<ListGroup.Item data-testid="tolerance-value"><b>Tolerance:</b> &#177;{stripes.toleranceValue}%</ListGroup.Item>
			    	<ListGroup.Item data-testid="minimum-value"><b>Minimum:</b> {stripes.minimum} ohms</ListGroup.Item>
			    	<ListGroup.Item data-testid="maximum-value"><b>Maximum:</b> {stripes.maximum} ohms</ListGroup.Item>
			 	</ListGroup>
			   </Card>
	        
	      	</Container>
		</div>	
	)

}