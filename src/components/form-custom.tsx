import { Component, useState } from 'react';
import { useForm } from "react-hook-form";

//styles
import Form from 'react-bootstrap/form';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Button from 'react-bootstrap/Button';

//import 'bootstrap/dist/css/bootstrap.min.css';


type Inputs = {
  	first: number,
	second: number,
	multiplier: number,
	tolerance: number
};

export default function FormCustom(props: any){

	//const [apiResponse, setState] = useState({ apiResponse: ""});
	const [validated, setValidated] = useState(false);

	const state = {
		first: 0,
		second: 0,
		multiplier: 0,
		tolerance: 0
	}

	const callAPI = (colors: any) => {
		let requestOptions = {
	        method: 'POST',
	        headers: { 'Content-Type': 'application/json' },
	        body: JSON.stringify(colors)
	    };
    	fetch("http://localhost:9000/testAPI", requestOptions)
        	.then(res => res.json())
        	.then(data => {
        		// console.log("data", data);
        		props.colorsData(data);
        		// setState({ apiResponse: res })
        	});
  	}

	

	const clearForm = (data: any) => {
		setValue("first", 0);
		setValue("second", 0);
		setValue("multiplier", 0);
		setValue("tolerance", 0);
	}

	
	const { register, handleSubmit, formState: { errors }, setValue } = useForm<Inputs>();

	const getColors = (data: any) =>{ 

		//console.log("data", data);
		let colors = {
			first: '',
			second: '',
			multiplier: '',
			tolerance: '',
			resistance: 0,
			toleranceValue: 0,
			minimum: 0,
			maximum: 0
		}
		// first digit
		switch(parseInt(data.first)){
			case 1: { colors.first = 'brown'; break;};
			case 2: { colors.first = 'red'; break;};
			case 3: { colors.first = 'orange'; break;};
			case 4: { colors.first = 'yellow'; break;};
			case 5: { colors.first = 'green'; break;};
			case 6: { colors.first = 'blue'; break;};
			case 7: { colors.first = 'violet'; break;};
			case 8: { colors.first = 'gray'; break;};
			case 9: { colors.first = 'white'; break;};
			default: { colors.first = 'invalid value'; break;};
		}

		// second digit
		switch(parseInt(data.second)){
			case 0: { colors.second = 'black'; break;};
			case 1: { colors.second = 'brown'; break;};
			case 2: { colors.second = 'red'; break;};
			case 3: { colors.second = 'orange'; break;};
			case 4: { colors.second = 'yellow'; break;};
			case 5: { colors.second = 'green'; break;};
			case 6: { colors.second = 'blue'; break;};
			case 7: { colors.second = 'violet'; break;};
			case 8: { colors.second = 'gray'; break;};
			case 9: { colors.second = 'white'; break;};
			default: { colors.second = 'invalid value'; break;};
		}

		// multiplier
		switch(parseFloat(data.multiplier)){
			case 1: { colors.multiplier = 'black'; break;};
			case 10: { colors.multiplier = 'brown'; break;};
			case 100: { colors.multiplier = 'red'; break;};
			case 1000: { colors.multiplier = 'orange'; break;};
			case 10000: { colors.multiplier = 'yellow'; break;};
			case 100000: { colors.multiplier = 'green'; break;};
			case 1000000: { colors.multiplier = 'blue'; break;};
			case 10000000: { colors.multiplier = 'violet'; break;};
			case 100000000: { colors.multiplier = 'gray'; break;};
			case 1000000000: { colors.multiplier = 'white'; break;};
			case 0.1: { colors.multiplier = 'gold'; break;};
			case 0.01: { colors.multiplier = 'silver'; break;};
			default: { colors.multiplier = 'invalud value'; break;};
		}

		// tolerance
		switch(parseFloat(data.tolerance)){
			case 1: { colors.tolerance = 'brown'; break;};
			case 2: { colors.tolerance = 'red'; break;};
			case 3: { colors.tolerance = 'orange'; break;};
			case 4: { colors.tolerance = 'yellow'; break;};
			case 0.5: { colors.tolerance = 'green'; break;};
			case 0.25: { colors.tolerance = 'blue'; break;};
			case 0.1: { colors.tolerance = 'violet'; break;};
			case 0.05: { colors.tolerance = 'gray'; break;};
			case 5: { colors.tolerance = 'gold'; break;};
			case 10: { colors.tolerance = 'silver'; break;};
			default: { colors.tolerance = 'invalid value'; break;};
		}

		//console.log("colors", colors);
		colors.resistance = parseInt("" + (data.first + data.second) * data.multiplier);
		colors.toleranceValue = parseFloat(data.tolerance);
		colors.minimum = colors.resistance - (colors.resistance * (colors.toleranceValue / 100));
		colors.maximum =  colors.resistance + (colors.resistance * (colors.toleranceValue / 100));
		callAPI(colors);
		
		//return colors
	}

	return (
		<div>
			<Jumbotron >
		        <Form noValidate validated={validated} inline onSubmit={handleSubmit(getColors)}>

		            <Form.Group controlId="first" className="col-12 col-md-6 mb-1">
		              <Form.Label className="col-4">1st digit: </Form.Label>
		              <Form.Control className="col-12 col-md-8" type="number" placeholder="Enter 1st digit" {...register("first", { required: true, min: 0, max: 9})} />
		              <Form.Text className="text-muted">
		                Numbers between 1 and 9 only.
		              </Form.Text>
		            </Form.Group>

		            <Form.Group controlId="second" className="col-12 col-md-6 mb-1">
		              <Form.Label className="col-4">2nd digit: </Form.Label>
		              <Form.Control className="col-12 col-md-8" type="number" placeholder="Enter 2nd digit"  {...register("second", { required: true, min: 0, max:9})} />
		              <Form.Text className="text-muted">
		                Numbers between 0 and 9 only.
		              </Form.Text>
		            </Form.Group>

		            <Form.Group controlId="multiplier" className="col-12 col-md-6 mb-1">
		              <Form.Label className="col-4">Multiplier: </Form.Label>
		              <Form.Control className="col-12 col-md-8" type="number" placeholder="Enter multiplier of the resistor" {...register("multiplier", { required: true, min: 0.01, max: 1000000000})} />
		              <Form.Text className="text-muted">
		                Multipliers of 10 only ex. 100, 100000.
		              </Form.Text>
		            </Form.Group>

		            <Form.Group controlId="tolerance" className="col-12 col-md-6 mb-1">
		              <Form.Label className="col-4">Tolerance: </Form.Label>
		              <Form.Control className="col-12 col-md-8" type="number" placeholder="Enter tolerance of the resistor" {...register("tolerance", { required: true, min: 0.05, max: 10})}  />
		              <Form.Text className="text-muted">
		                Numbers between 0.05 and 10 only.
		              </Form.Text>
		            </Form.Group> 
		              
		          <Button className="ml-3 mr-3 mt-4" type="submit" variant="success">Get colors</Button>{' '}
		          <Button onClick={clearForm} className="ml-3 mr-3 mt-4" type="button" variant="secondary">Clear form</Button>{' '}
		        </Form>
		        
		    </Jumbotron>
		</div>		
	);
}