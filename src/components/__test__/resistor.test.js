import React from 'react'
import ReactDOM from 'react-dom';
import Resistor from '../resistor';

import {render, cleanup} from '@testing-library/react';

afterEach(cleanup);
const colors = {
			first: 'brown',
			second: 'black',
			multiplier: 'red',
			tolerance: 'silver',
			resistance: 1000,
			toleranceValue: 10,
			minimum: 900,
			maximum: 1100
		}
it("renders the first color", ()=>{
	const {getByTestId} = render(<Resistor colorStripes={colors}></Resistor>)
	expect(getByTestId('first-color')).toHaveClass('brown');
})

it("renders the second color", ()=>{
	const {getByTestId} = render(<Resistor colorStripes={colors}></Resistor>)
	expect(getByTestId('second-color')).toHaveClass('black');
})

it("renders the third color", ()=>{
	const {getByTestId} = render(<Resistor colorStripes={colors}></Resistor>)
	expect(getByTestId('third-color')).toHaveClass('red');
})

it("renders the fourth color", ()=>{
	const {getByTestId} = render(<Resistor colorStripes={colors}></Resistor>)
	expect(getByTestId('fourth-color')).toHaveClass('silver');
})

it("renders the resistance value", ()=>{
	const {getByTestId} = render(<Resistor colorStripes={colors}></Resistor>)
	expect(getByTestId('resistance-value')).toHaveTextContent(1000);
})

it("renders the tolerance value", ()=>{
	const {getByTestId} = render(<Resistor colorStripes={colors}></Resistor>)
	expect(getByTestId('tolerance-value')).toHaveTextContent(10);
})

it("renders the minimum value", ()=>{
	const {getByTestId} = render(<Resistor colorStripes={colors}></Resistor>)
	expect(getByTestId('minimum-value')).toHaveTextContent(900);
})

it("renders the maximum value", ()=>{
	const {getByTestId} = render(<Resistor colorStripes={colors}></Resistor>)
	expect(getByTestId('maximum-value')).toHaveTextContent(1100);
})
