import React, {useState} from 'react';



//styles
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';



//components
import FormCustom from './components/form-custom';
import Resistor from './components/resistor'




function App() {

  const [resistorColors, setColors] = useState({});


  const getColors = (colors: any) => {
    setColors(colors) 
  }

  return (
    <div>

      <FormCustom colorsData={getColors}></FormCustom>
      <Resistor colorStripes={resistorColors}></Resistor>
      
    </div>
  );
}

export default App;
