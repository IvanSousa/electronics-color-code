var express = require("express");
var router = express.Router();

router.get("/", function(req, res, next) {

    res.send("API is working properly");
});

router.post('/', function (req, res) {

	let colorsRes = req.body;
	console.log("colors", colorsRes);
  res.send(colorsRes);
});

module.exports = router;